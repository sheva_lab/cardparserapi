package org.shevchenko.cardparser.exceptions;

public class CardFileException extends RuntimeException {

    public CardFileException(String message) {
        super(message);
    }
}
