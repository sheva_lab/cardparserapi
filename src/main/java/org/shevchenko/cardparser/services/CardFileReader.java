package org.shevchenko.cardparser.services;

import org.shevchenko.cardparser.entities.Card;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.shevchenko.cardparser.utils.CardFileUtil.mapToCard;

@Component
public class CardFileReader implements ICardFileReader {

    @Override
    public List<Card> readCardFile(Reader source) {
        try (BufferedReader bufferedReader = new BufferedReader(source)) {
            return bufferedReader.lines()
                    .skip(1)
                    .map(mapToCard)
                    .collect(toList());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
