package org.shevchenko.cardparser.services;

import org.shevchenko.cardparser.entities.Card;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

public interface ICardsService {

     List<Card> getAllCardsFromFile(InputStream inputStream) throws FileNotFoundException;

     List<Card> getCardsFromFile(InputStream inputStream, long limit) throws FileNotFoundException ;

     List<Card> getCardsWithoutErrors(List<Card>cards);
}
