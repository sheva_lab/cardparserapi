package org.shevchenko.cardparser.services;

import org.shevchenko.cardparser.entities.Card;
import org.shevchenko.cardparser.utils.CardFileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CardsService implements ICardsService {

    @Autowired
    ICardFileReader cardFileReader;

    @Override
    public List<Card> getAllCardsFromFile(InputStream inputStream) {
        return cardFileReader.readCardFile(new InputStreamReader(inputStream));
    }

    @Override
    public List<Card> getCardsFromFile(InputStream inputStream, long limit) {
        return cardFileReader.readCardFile(new InputStreamReader(inputStream))
                .stream()
                .limit(limit)
                .collect(Collectors.toList());
    }

    @Override
    public List<Card> getCardsWithoutErrors(List<Card> cards) {
        List<Card> filteredCards = cards.stream()
                .filter(CardFileUtil.combineCardFilters(CardFileUtil.isWithDate, CardFileUtil.isWithCardType, CardFileUtil.isWithoutSalesError))
                .collect(Collectors.toList());

        return filteredCards.stream().map(CardFileUtil.emptyCardPayloadCountToAvg(filteredCards)).collect(Collectors.toList());
    }
}
