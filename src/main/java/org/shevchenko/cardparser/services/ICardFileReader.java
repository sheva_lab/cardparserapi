package org.shevchenko.cardparser.services;

import org.shevchenko.cardparser.entities.Card;

import java.io.Reader;
import java.util.List;

public interface ICardFileReader {

    List<Card> readCardFile(Reader source);
}
