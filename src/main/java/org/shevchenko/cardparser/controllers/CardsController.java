package org.shevchenko.cardparser.controllers;

import io.swagger.annotations.*;
import org.shevchenko.cardparser.entities.Card;
import org.shevchenko.cardparser.entities.CardResponse;
import org.shevchenko.cardparser.services.ICardsService;
import org.shevchenko.cardparser.utils.CardFileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;


@Api(value = "Cards")
@RequestMapping("/cards")
@RestController
public class CardsController {

    private static final Logger logger = LoggerFactory.getLogger(CardsController.class);

    @Autowired
    ICardsService cardsService;

    @ApiOperation(value = "Make a POST request to upload card file",
            produces = "application/json", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The POST call is Successful"),
            @ApiResponse(code = 500, message = "The POST call is Failed"),
            @ApiResponse(code = 404, message = "The API could not be found")
    })
    @PostMapping
    public ResponseEntity<CardResponse> getAllCardsFromFile(@ApiParam(name = "file", value = "Select the card file to Upload", required = true)
                                                            @RequestParam("file") MultipartFile file) throws IOException {

        CardFileUtil.checkUploadedFile(file);

        List<Card> cards = cardsService.getAllCardsFromFile(file.getInputStream());
        List<Card> cardsWithoutErrors = cardsService.getCardsWithoutErrors(cards);

        int errors = cards.size() - cardsWithoutErrors.size();

        logger.info("card file has been uploaded successfully");

        return new ResponseEntity<>(new CardResponse(cardsWithoutErrors, errors), HttpStatus.OK);
    }
}
