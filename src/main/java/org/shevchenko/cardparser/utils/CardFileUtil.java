package org.shevchenko.cardparser.utils;

import org.shevchenko.cardparser.entities.Card;
import org.shevchenko.cardparser.exceptions.CardFileException;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public final class CardFileUtil {

    private static final String SEPARATOR = ",";
    private static final String CONTENT_TYPE = "text/csv";

    private static final String REQUIRED_CARD_FILE_MESSAGE = "You must select card file for uploading";
    private static final String EMPTY_CARD_FILE_MESSAGE = "You must upload not empty card file";
    private static final String CARD_FILE_MIME_TYPE_MESSAGE = "You must upload card file in csv format";

    //temporary flags
    private static long DEFAULT_SALES_EMPTY = -1;
    private static long DEFAULT_SALES_ERROR = -2;

    public static Function<String, Card> mapToCard = (line) -> {
        String[] lines = line.split(SEPARATOR);
        boolean isColumnExist = lines.length == 3;

        String date = isColumnExist ? lines[0].trim() : "";
        String cardType = isColumnExist ? lines[1].trim() : "";
        long salesCount = isColumnExist ? setSalesCount(lines[2]) : DEFAULT_SALES_EMPTY;
        return new Card(date, cardType, salesCount);
    };

    private static Optional<Long> parseSalesCount(String data) {
        return Optional.of(data.trim())
                .map(str -> str.isEmpty() ? String.valueOf(DEFAULT_SALES_EMPTY) : str)
                .filter(StringUtil::isNumeric)
                .map(Long::parseLong);
    }

    private static long setSalesCount(String data) {
        Optional<Long> salesCount = parseSalesCount(data);
        return salesCount.orElseGet(() -> DEFAULT_SALES_ERROR);
    }

    public static Predicate<Card> isWithoutSalesError = card -> card.getSalesCount() != DEFAULT_SALES_ERROR;
    public static Predicate<Card> isWithDate = card -> StringUtil.isDate(card.getDate());
    public static Predicate<Card> isWithCardType = card -> !card.getCardType().isEmpty();

    @SafeVarargs
    public static <T> Predicate<T> combineCardFilters(Predicate<T>... predicates) {
        return Stream.of(predicates).reduce(x -> true, Predicate::and);
    }

    public static Function<Card, Card> emptyCardPayloadCountToAvg(List<Card> cards) {
        return card -> {
            String cardType = card.getCardType();
            if (card.getSalesCount() == DEFAULT_SALES_EMPTY) card.setSalesCount(modifyEmptyToAvg(cards, cardType));
            return card;
        };
    }

    private static long modifyEmptyToAvg(List<Card> cards, String cardType) {
        OptionalDouble average = cards.stream()
                                      .filter(card -> card.getCardType().equalsIgnoreCase(cardType) && card.getSalesCount() != DEFAULT_SALES_EMPTY)
                                      .mapToLong(Card::getSalesCount)
                                      .average();
        return Math.round(average.isPresent() ? average.getAsDouble() : 0);
    }

    public static void checkUploadedFile(MultipartFile multipartFile) {
        if (multipartFile == null) {
            throw new CardFileException(REQUIRED_CARD_FILE_MESSAGE);
        } else if (multipartFile.isEmpty()) {
            throw new CardFileException(EMPTY_CARD_FILE_MESSAGE);
        } else if (!Objects.requireNonNull(multipartFile.getContentType()).equals(CONTENT_TYPE)) {
            throw new CardFileException(CARD_FILE_MIME_TYPE_MESSAGE);
        }
    }
}
