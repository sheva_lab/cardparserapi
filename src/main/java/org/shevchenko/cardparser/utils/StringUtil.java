package org.shevchenko.cardparser.utils;

public final class StringUtil {

    private static final String DIGIT_REGEX = "[+]?\\d*(\\.\\d+)?";
    private static final String DATE_REGEX = "^(1[0-2]|0[1-9]).(3[01]|[12][0-9]|0[1-9]).[0-9]{4}$";

    public static boolean isNumeric(String data) {
        if (data.startsWith("-")) return data.substring(1).matches(DIGIT_REGEX);
        return data.matches(DIGIT_REGEX);
    }

    public static boolean isDate(String data) {
        return data.matches(DATE_REGEX);
    }
}
