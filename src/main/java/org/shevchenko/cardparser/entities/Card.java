package org.shevchenko.cardparser.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@AllArgsConstructor
public class Card {

    @Getter
    private String date;

    @Getter
    private String cardType;

    @Setter
    @Getter
    private long salesCount;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return salesCount == card.salesCount &&
                Objects.equals(date, card.date) &&
                Objects.equals(cardType, card.cardType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, cardType, salesCount);
    }
}
