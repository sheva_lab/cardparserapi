package org.shevchenko.cardparser.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
public class CardResponse {

    @Getter
    private List<Card> cards;

    @Getter
    private long errors;
}
