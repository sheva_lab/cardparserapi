package org.shevchenko.cardparser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CardParser {

    public static void main(String[] args) {
        SpringApplication.run(CardParser.class, args);
    }

}
